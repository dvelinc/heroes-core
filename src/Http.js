'use strict';

/**
 * wrapper for res.send to hook every response
 *
 * usage:
 * heroesCore.Http.send(req, res, { statusCode: 200 }); // OK
 * heroesCore.Http.send(req, res, { statusCode: 200, body: { key: value } }); // OK
 * heroesCore.Http.send(req, res, { statusCode: 500, body: { error: 'failed error message' } }); // ERROR
 *
 * @param req
 * @param res
 * @param options object { statusCode, headers, body }
 */
exports.send = function send(req, res, options) {
  if (!req) {
    throw new Error('Http.send: req is missing');
  }
  if (!res) {
    throw new Error('Http.send: res is missing');
  }
  if (!options) {
    throw new Error('Http.send: options is missing');
  }
  if (!options.statusCode) {
    throw new Error('Http.send: options.statusCode is missing');
  }
  // set http status code
  res.status(options.statusCode);
  // set headers
  if (options.headers) {
    Object.keys(options.headers).forEach((header) => {
      res.setHeader(header, options.headers[header]);
    });
  }
  // send back
  if (req.method === 'HEAD') {
    res.send(''); // HEAD must not has a body
  } else {
    res.send(options.body);
  }
  // logging
  if ((options.statusCode < 200 || options.statusCode > 299) && options.statusCode !== 404) {
    console.error(options.statusCode, '|', options.body, '|', req.url);
  }
};
