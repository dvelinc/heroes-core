'use strict';

const chrono = require('chrono-node');

/**
 * Global Helpers
 */
module.exports = {
  /**
   * Takes a session object, an array to get a unique random index for and a name for the memory variable
   * Returns an unique index of array and saves used indices to sessionStore.usedIndices.<name>[]
   * @param sessionStore object
   * @param array array[*]
   * @param name string
   * @returns number
   */
  getUniqueRandomArrayIndex: function getUniqueRandomArrayIndex(sessionStore, array, name) {
    if (!sessionStore || typeof sessionStore !== 'object') {
      throw new Error(
        `helpers.getUniqueRandomArrayIndex: sessionStore must be of type object, is ${typeof sessionStore}!`);
    }
    if (!Array.isArray(array)) {
      throw new Error(`helpers.getUniqueRandomArrayIndex: array must be of type array, is ${typeof array}!`);
    }
    if (!name) {
      throw new Error('helpers.getUniqueRandomArrayIndex: name must be a string!');
    }
    if (!sessionStore.usedIndices) {
      sessionStore.usedIndices = {};
    }
    // get a unique random index (persists while repeating this dialog)
    let index = module.exports.getRandomArrayKey(array);

    if (sessionStore.usedIndices[name]) {
      if (sessionStore.usedIndices[name].length === array.length) {
        sessionStore.usedIndices[name] = [];
      }
      while (sessionStore.usedIndices[name].includes(index)) {
        index = module.exports.getRandomArrayKey(array);
      }
      sessionStore.usedIndices[name].push(index);
    } else {
      sessionStore.usedIndices[name] = [index];
    }
    return index;
  },
  /**
   * Gets a random key from an array
   * @param array array[*]
   * @returns number
   */
  getRandomArrayKey: function getRandomArrayKey(array) {
    return Math.floor(Math.random() * array.length);
  },
  /**
   * Checks if complex or primitive variables are empty
   * @param variable object, array, string, number, undefined
   * @returns boolean
   */
  isEmpty: function isEmpty(variable) {
    switch (typeof variable) {
      case 'object':
        if (Array.isArray(variable)) {
          return variable.length === 0;
        }
        if (!variable) {
          return true;
        }
        return Object.keys(variable).length === 0 && variable.constructor === Object;
      case 'string':
        return variable === '' || variable === 'null';
      case 'number':
        return variable === 0;
      case 'undefined':
        return true;
      default:
        throw new Error(`helpers.isEmpty: object has unsupported type (${typeof variable})`);
    }
  },
  /**
   * Deep clones a object by value (not inheriting prototype)
   * @param object object
   * @returns object
   */
  cloneObjectByValue: function cloneObjectByValue(object) {
    if (!object || typeof object !== 'object') {
      throw new Error(
        `helpers.cloneObjectByValue: object must be of type object, is ${object === null ? 'null' : typeof object}`);
    }
    if (object instanceof Date) {
      throw new Error('helpers.cloneObjectByValue: date objects are not supported!');
    }
    return JSON.parse(JSON.stringify(object));
  },
  /**
   * Get values of a single-level object
   * @param object
   * @return Array
   */
  getObjectValues: function getObjectValues(object) {
    return Object.keys(object).map((key) => object[key]);
  },
  /**
   * Removes every ASCII sign with an unicode higher than 126 from a string, removes hashes, lowercases and trims it
   * @param asciiString string
   * @param replacement optional string
   * @returns string
   */
  truncateBaseLatinAsciiString: function truncateBaseLatinAsciiString(asciiString, replacement) {
    if (typeof asciiString !== 'string') {
      throw new Error(
        `helpers.truncateBaseLatinAsciiString: acsiiString must be of type string, is ${typeof asciiString}`);
    }
    const result = [];
    if (asciiString.length > 1) {
      asciiString.split('').forEach((character) => {
        if (character.charCodeAt(0) < 126) { // note: 126 is the last char in the Base Latin ASCII signs
          result.push(character);
        } else if (replacement) {
          result.push(replacement);
        }
      });
    } else if (asciiString.length === 1) {
      if (asciiString.charCodeAt(0) < 126) { // note: 126 is the last char in the Base Latin ASCII signs
        result.push(asciiString);
      } else if (replacement) {
        result.push(replacement);
      }
    }
    return result
      .join('')
      .replace(/#/g, '')
      .replace(/_/g, ' ')
      .trim()
      .toLowerCase();
  },
  /**
   * Parses a Date Object out of a String containing phrases like 'next monday, 8am..'
   * @param dateString string
   * @returns object {type: sting, entity: string, index: number, endIndex: number,
   * resolution: { resolution_type: string, start: date, end: date, ref: string }, score: float }, boolean
   */
  parseTime: function parseTime(dateString) {
    const resultTime = chrono.parse(dateString);
    if (resultTime && resultTime.length > 0) {
      const duration = resultTime[0];
      const response = {
        type: 'chrono.duration',
        entity: duration.text,
        startIndex: duration.index,
        endIndex: duration.index + duration.text.length,
        resolution: {
          resolution_type: 'chrono.duration',
          start: duration.start.date(),
        },
      };
      if (duration.end) {
        response.resolution.end = duration.end.date();
      }
      if (duration.ref) {
        response.resolution.ref = duration.ref;
      }
      response.score = response.entity.length / dateString.length;
      return response;
    }
    return false;
  },
  /**
   * Remove colon and convert a time string to number
   * @param hhmm string format: 'hh:mm' '22:00'
   * @returns {number}
   */
  stripColonAndConvertToNumber: function stripColonAndConvertToNumber(hhmm) {
    if (typeof hhmm !== 'string') {
      throw new Error(`hhmm must be of type string, is ${typeof hhmm}`);
    }
    return Number(hhmm.replace(':', '').trim());
  },
  /**
   * Insert a colon to a truncated time string and convert it to string
   * @param hhmm integer format: 'hhmm' '2200'
   * @returns {string}
   */
  insertColonAndConvertToString: function insertColonAndConvertToString(hhmm) {
    if (typeof hhmm !== 'string') {
      throw new Error(`hhmm must be of type string, is ${typeof hhmm}`);
    }
    if (hhmm.includes(':')) {
      throw new Error('Helpers.insertColonAndConvertToString: hhmm has got a colon already');
    }
    switch (hhmm.length) {
      case 4:
        return String(`${hhmm.slice(0, 2)}:${hhmm.slice(2)}`);
      case 3:
        return String(`${hhmm.slice(0, 1)}:${hhmm.slice(1)}`);
      case 2:
        return `00:${String(hhmm)}`;
      case 1:
        return `00:0${String(hhmm)}`;
      default:
        throw new Error(`Helpers.insertColonAndConvertToString: hhmm got a bad format: ${hhmm}`);
    }
  },
  /**
   * Calculates the UTC of a time string by a given timezone
   * @param time string
   * @param timezone number
   * @param day number optional If set, the UTC day will be returned
   * @returns object { hours:number, minutes:number, time:string, dayOffset:number, day:number }
   */
  getUTCTime: function getUTCTime(time, timezone, day) {
    if (typeof time !== 'string') {
      throw new Error(`Helpers.getUTCTime: time must be of type string, is ${typeof time}.`);
    }
    if (!module.exports.validateTime24h(time)) {
      throw new Error(`Helpers.getUTCTime: time must be in H:M format, is ${time}.`);
    }
    if (!timezone) {
      timezone = 0;
    }
    if (timezone > 12 || timezone < -12) {
      throw new Error(`Helpers.getUTCTime: invalid timezone: ${timezone}`);
    }
    const timeArray = time.split(':');
    const dateTime = new Date();
    const currentDay = dateTime.getDay();
    dateTime.setHours(parseInt(timeArray[0], 10) - timezone);
    dateTime.setMinutes(parseInt(timeArray[1], 10));
    let offsetDay;
    if (typeof day === 'number') {
      if (day < 0 || day > 6) {
        throw new Error(`Helpers.getUTCTime: day must be between 0 and 6, is ${day}`);
      }
      offsetDay = day + (dateTime.getDay() - currentDay);
      if (offsetDay < 0) {
        offsetDay = 7 + offsetDay;
      }
      if (offsetDay > 6) {
        offsetDay -= 7;
      }
    }
    return {
      hours: dateTime.getHours(),
      minutes: dateTime.getMinutes(),
      day: offsetDay,
      dayOffset: dateTime.getDay() - currentDay,
      time: `${dateTime.getHours()}:${(String(
        dateTime.getMinutes())).length > 1 ? dateTime.getMinutes() : `0${dateTime.getMinutes()}`}`,
    };
  },
  /**
   * Calculates the local time of a UTC time string by a given timezone
   * @param time string
   * @param timezone number
   * @param day number optional If set, the UTC day will be returned
   * @returns object { hours:number, minutes:number, time:string, dayOffset:number, day:number }
   */
  getLocalTime: function getLocalTime(time, timezone, day) {
    if (typeof time !== 'string') {
      throw new Error(`Helpers.getLocalTime: time must be of type string, is ${typeof time}.`);
    }
    if (!module.exports.validateTime24h(time)) {
      throw new Error(`Helpers.getLocalTime: time must be in H:M format, is ${time}.`);
    }
    if (!timezone) {
      timezone = 0;
    }
    if (timezone > 12 || timezone < -12) {
      throw new Error(`Helpers.getUTCTime: invalid timezone: ${timezone}`);
    }
    const timeArray = time.split(':');
    const dateTime = new Date();
    const currentDay = dateTime.getDay();
    dateTime.setHours(parseInt(timeArray[0], 10) + timezone);
    dateTime.setMinutes(parseInt(timeArray[1], 10));
    let offsetDay;
    if (typeof day === 'number') {
      if (day < 0 || day > 6) {
        throw new Error(`Helpers.getLocalTime: day must be between 0 and 6, is ${day}`);
      }
      offsetDay = day + (dateTime.getDay() - currentDay);
      if (offsetDay < 0) {
        offsetDay = 7 + offsetDay;
      }
      if (offsetDay > 6) {
        offsetDay -= 7;
      }
    }
    return {
      hours: dateTime.getHours(),
      minutes: dateTime.getMinutes(),
      day: offsetDay,
      dayOffset: dateTime.getDay() - currentDay,
      time: `${dateTime.getHours()}:${(String(
        dateTime.getMinutes())).length > 1 ? dateTime.getMinutes() : `0${dateTime.getMinutes()}`}`,
    };
  },
  /**
   * Adds leading zeros to time strings. If timezone is given, the UTC time will be used
   * @param options.time string
   * @param options.timezone optional number
   * @param options.convert optional string[local|utc]
   * @param options.format optional string[12h]
   * @returns string time
   */
  serializeTime: function serializeTime(options) {
    if (typeof options.time !== 'string') {
      throw new Error(`Helpers.serializeTime: time must be of type string, is ${typeof options.time}.`);
    }
    if (!module.exports.validateTime24h(options.time)) {
      throw new Error(`Helpers.serializeTime: time must be in H:M format, is ${options.time}.`);
    }
    let hours;
    let minutes;
    let timeAppendix = '';
    let timeData;
    if (options.convert === 'local') {
      timeData = module.exports.getLocalTime(options.time, options.timezone);
      hours = timeData.hours;
      minutes = timeData.minutes;
    } else if (options.convert === 'utc') {
      timeData = module.exports.getUTCTime(options.time, options.timezone);
      hours = timeData.hours;
      minutes = timeData.minutes;
    } else {
      timeData = options.time.split(':');
      hours = timeData[0];
      minutes = timeData[1];
    }
    hours = parseInt(hours, 10);
    minutes = parseInt(minutes, 10);
    if (options.format === '12h') {
      if (hours > 12) {
        hours -= 12;
        timeAppendix = 'pm';
      } else {
        timeAppendix = 'am';
      }
      if (!minutes) {
        return `${hours} ${timeAppendix}`.trim();
      }
    }
    return `${hours}:${String(minutes).length > 1 ? minutes : `0${minutes}`} ${timeAppendix}`.trim();
  },
  /**
   * Validates time strings in 24h format
   * @param time string Possible values: HH:MM, HH:M, H:MM, H:M
   * @returns boolean
   */
  validateTime24h: function validateTime24h(time) {
    if (typeof time !== 'string') {
      throw new Error(`Helpers.validateTime24h: time must be of type string, is ${typeof time}.`);
    }
    return time.match(/(\d|[01]\d|2[0-3]):(\d|[0-5]\d)/);
  },
  /**
   * Validates time strings in 12h format
   * @param time string Possible values: H pm, Hpm, HHpm, HH am, H:M pm, HH:MM am
   * @returns boolean
   */
  validateTime12h: function validateTime12h(time) {
    if (typeof time !== 'string') {
      throw new Error(`Helpers.validateTime12h: time must be of type string, is ${typeof time}.`);
    }
    return time.match(/(1[012]|[\d])(:[0-5][\d])?( )?(am|pm)/);
  },
  /**
   * Converts a numeric index to it's corresponding weekday
   * 0 = sunday, 6 = saturday
   * @param index number
   * @param language string
   * @returns string weekDay
   */
  convertWeekdayIndexToLiteral: function convertWeekdayIndexToLiteral(index, language) {
    switch (language) {
      case 'en':
      default:
        switch (index) {
          case 1:
            return 'monday';
          case 2:
            return 'tuesday';
          case 3:
            return 'wednesday';
          case 4:
            return 'thursday';
          case 5:
            return 'friday';
          case 6:
            return 'saturday';
          case 0:
            return 'sunday';
          default:
            throw new Error(`wrong index entered for serialization '${index}'`);
        }
    }
  },
  /**
   * Captializes a string
   * @param string string
   * @returns string string
   */
  capitalize: function capitalize(string) {
    if (typeof string !== 'string') {
      throw new Error('helpers.capitalize: string must be of type string');
    }
    return `${string[0].toUpperCase()}${string.substr(1).toLowerCase()}`;
  },
  /**
   * Randomly shuffles the keys of an array
   * @param array Array
   */
  shuffleArray: function shuffle(array) {
    for (let i = array.length; i; i -= 1) {
      const j = Math.floor(Math.random() * i);
      [array[i - 1], array[j]] = [array[j], array[i - 1]];
    }
  },
  /**
   * Checks if an array matches another array's values in the same order
   * @param array1 Array
   * @param array2 Array
   * @return {boolean}
   */
  arraysAreEqual: function arraysAreEqual(array1, array2) {
    if (!Array.isArray(array1) || !Array.isArray(array2)) {
      throw new Error(`array1 and array2 must be of type array, are: ${typeof array1}, ${typeof array2}`);
    }
    for (let i = 0; i < array1.length - 1; i += 1) {
      if (array1[i] !== array2[i]) {
        return false;
      }
    }
    return true;
  },
  /**
   * Returns a random time between 2 hours
   * @param hour1 number Starting hour
   * @param hour2 number Ending hour
   * @param format string optional possible values: '12h' Special format of returned time, defaults to 24h format
   * @return {string} time
   */
  getRandomTimeBetweenHours: function getRandomTimeBetweenHours(hour1, hour2, format) {
    const hours = [hour1];
    let hour = hour1;
    while (hour + 1 <= hour2) {
      hours.push(hour + 1);
      hour += 1;
    }
    let minutes = '00';
    const index = module.exports.getRandomArrayKey(hours);
    if (index !== hours.length - 1) {
      minutes = Math.floor(Math.random() * 59);
    }
    return module.exports.serializeTime({
      time: `${hours[index]}:${minutes}`,
      format,
    });
  },
  /**
   * Maps an object to GET parameters
   * @param params object { paramName: paramValue }
   * @return string ?paramName=paramValue&...
   */
  createQueryString: function createQueryString(params) {
    const result = Object.keys(params)
      .filter((param) => {
        if (params[param] && params[param].toString && params[param].toString() !== '[object Object]') {
          return params[param];
        }
        return null;
      })
      .map((param) => {
        return `${param}=${params[param].toString()}`;
      });
    return `?${result.join('&')}`;
  },
};
