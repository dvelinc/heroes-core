'use strict';

const helpers = require('../Helpers');
const builder = require('botbuilder');
const createQuickReplies = require('./quickReplies').createQuickReplies;

module.exports = {
  /**
   * Create facebook generic template messages
   * @param session object
   * @param content object
   * @param content.elements array[object]
   * @param content.elements.title string
   * @param content.elements.image_url string
   * @param content.elements.subtitle string
   * @param content.elements.text string
   * @param content.elements.buttons array
   * @param content.elements.buttons.type string
   * @param content.elements.buttons.title string
   * @param content.elements.buttons.msg string
   * @param content.elements.buttons.url string
   * @param content.elements.buttons.image_url string
   * @param content.quick_replies object
   * @param content.carouselSize integer
   */
  createCard: function createCard(session, content) {
    if (!Array.isArray(content.elements) || content.elements.length === 0) {
      throw new Error('createCard: elements must be a filled array!');
    }
    const carousel = [];
    content.elements.forEach((element) => {
      if (!element.title || !(typeof element.title === 'string' || Array.isArray(element.title))) {
        throw new Error('createCard: title of cards must be set!');
      }
      const card = new builder.HeroCard(session)
        .title(element.title);
      card.contentType = 'application/vnd.microsoft.card.hero';
      card.data.reusable = true;
      if (element.subtitle) {
        card.subtitle(element.subtitle);
      }
      if (element.text) {
        card.text(element.text);
      }
      if (element.image_url) {
        card.images([builder.CardImage.create(session, element.image_url)]);
      }
      if (Array.isArray(element.buttons) && element.buttons.length > 0) {
        const buttons = [];
        element.buttons.forEach((button) => {
          let action;
          switch (button.type) {
            case 'postback':
              action = builder.CardAction.postBack(session, button.payload, button.title);
              break;
            case 'openUrl':
              action = builder.CardAction.openUrl(session, button.url, button.title);
              break;
            case 'playAudio':
              action = builder.CardAction.playAudio(session, button.url, button.title);
              break;
            case 'playVideo':
              action = builder.CardAction.playVideo(session, button.url, button.title);
              break;
            case 'downloadFile':
              action = builder.CardAction.downloadFile(session, button.url, button.title);
              break;
            case 'imBack':
              action = builder.CardAction.imBack(session, button.payload, button.title);
              break;
            case 'element_share':
              buttons.push({ type: 'shareCard' });
              return;
            case 'webview':
              if (!button.webview_height_ratio || typeof button.webview_height_ratio !== 'string') {
                throw new Error('createCard: button.webview_height must be set when buttonType is \'webview\'!');
              }
              action = builder.CardAction.openUrl(session, button.url, button.title);
              action.data.messenger_extensions = true;
              action.data.webview_height_ratio = button.webview_height_ratio;
              if (button.fallback_url) {
                action.data.fallback_url = button.fallback_url;
              }
              buttons.push(action);
              return;
            default:
              throw new Error(`createCard: wrong button type '${button.type}'`);
          }
          if (button.image_url) {
            action.image(button.image_url);
          }
          buttons.push(action);
        });
        card.buttons(buttons);
      }
      carousel.push(card);
    });
    const attachments = carousel;
    if (content.quick_replies) {
      attachments.push(createQuickReplies(session, content.quick_replies));
    }
    return new builder.Message(session)
      .attachments(attachments)
      .attachmentLayout(content.carouselSize ? builder.AttachmentLayout.carousel : builder.AttachmentLayout.list);
  },
  /**
   * Create facebook generic template messages
   * @param session object
   * @param content object
   * @param content.elements array[object]
   * @param content.elements.title string
   * @param content.elements.item_url string
   * @param content.elements.image_url string
   * @param content.elements.subtitle string
   * @param content.elements.buttons object
   * @param content.elements.buttons.type string
   * @param content.elements.buttons.title string
   * @param content.elements.buttons.payload string
   * @param content.elements.buttons.url string
   * @param content.elements.buttons.webview_height_ratio string
   * @param content.elements.buttons.fallback_url string
   * @param content.quick_replies array[object]
   * @param content.quick_replies.content_type string
   * @param content.quick_replies.title string
   * @param content.quick_replies.payload string
   * @param content.quick_replies.image_url string
   */
  createCardFacebook: function createCardFacebook(session, content) {
    if (session.message.source !== 'facebook') {
      throw new Error('createCardFacebook: This function is supported in Facebook Messenger only.');
    }
    if (!Array.isArray(content.elements)) {
      throw new Error('createCardFacebook: content.elements must be an array.');
    }
    if (!content.elements || content.elements.length === 0) {
      throw new Error('createCardFacebook: content.elements may not be empty.');
    }
    // initialize message object
    const message = {
      facebook: {},
    };
    const attachment = {
      type: 'template',
      payload: {},
    };
    attachment.payload.template_type = 'generic';
    const elements = [];
    content.elements.forEach((element, elementsIndex) => {
      const newElement = {};
      if (!element.title) {
        throw new Error(`createCardFacebook: title of element ${elementsIndex} must be set.`);
      }
      if (Array.isArray(element.title)) {
        newElement.title = element.title[helpers.getRandomArrayKey(element.title)];
      } else {
        newElement.title = element.title;
      }
      if (element.item_url) {
        newElement.item_url = element.item_url;
      }
      if (Array.isArray(element.image_url)) {
        newElement.image_url = element.image_url[helpers.getRandomArrayKey(element.image_url)];
      } else if (typeof element.image_url === 'string') {
        newElement.image_url = element.image_url;
      }
      if (element.subtitle) {
        newElement.subtitle = element.subtitle;
      }
      if (element.buttons && !Array.isArray(element.buttons)) {
        throw new Error(`createCardFacebook: buttons of element ${element.title} must be an array.`);
      }
      if (element.buttons) {
        const buttons = [];
        element.buttons.forEach((button, buttonsIndex) => {
          const newButton = {};
          switch (button.type) {
            case 'element_share':
              // share button
              newButton.type = button.type;
              break;
            case 'postback':
              // postback button
              if (!button.title) {
                throw new Error(
                  `createCardFacebook: button ${buttonsIndex} of element ${element.title} is of type 'postback' but has no title set.`); // eslint-disable-line max-len
              }
              if (!button.payload) {
                throw new Error(
                  `createCardFacebook: button ${buttonsIndex} of element ${element.title} is of type 'postback' but has not payload set.`); // eslint-disable-line max-len
              }
              newButton.type = button.type;
              newButton.title = button.title;
              newButton.payload = button.payload;
              break;
            default:
              // web-url button
              if (!button.title) {
                throw new Error(
                  `createCardFacebook: button ${buttonsIndex} of element ${element.title} is of type 'web-url' but has no title set.`); // eslint-disable-line max-len
              }
              if (!button.url) {
                throw new Error(
                  `createCardFacebook: button ${buttonsIndex} of element ${element.title} is of type 'web-url' but has no url set.`); // eslint-disable-line max-len
              }
              newButton.type = 'web_url';
              newButton.title = button.title;
              newButton.url = button.url;
              // webview button
              if (button.webview_height_ratio) {
                newButton.webview_height_ratio = button.webview_height_ratio;
                newButton.messenger_extensions = true;
              }
              if (button.fallback_url) {
                newButton.fallback_url = button.fallback_url;
              }
          }
          buttons.push(newButton);
        });
        newElement.buttons = buttons;
      }
      elements.push(newElement);
    });
    attachment.payload.elements = elements;
    if (content.quick_replies && !Array.isArray(content.quick_replies)) {
      throw new Error('card.createCardFacebook: quick_replies must be an array.');
    }
    if (content.quick_replies) {
      const quickReplies = [];
      content.quick_replies.forEach((quickReply) => {
        const newQuickReply = {};
        switch (quickReply.content_type) {
          case 'location':
            newQuickReply.content_type = quickReply.content_type;
            break;
          default:
            newQuickReply.content_type = 'text';
            if (!quickReply.title) {
              throw new Error('card.createCardFacebook: quick replies of type text must contain a title property.');
            }
            if (!quickReply.payload) {
              throw new Error('card.createCardFacebook: quick replies of type text must contain a payload property.');
            }
            newQuickReply.title = quickReply.title;
            newQuickReply.payload = quickReply.payload;
            if (quickReply.image_url) {
              newQuickReply.image_url = quickReply.image_url;
            }
        }
        quickReplies.push(newQuickReply);
      });
      message.facebook.quick_replies = quickReplies;
    }
    message.facebook.attachment = attachment;
    return new builder.Message(session)
      .sourceEvent(message);
  },
  /**
   * Create generic video cards
   * @param session object
   * @param content object
   * @param content.title string
   * @param content.subtitle string
   * @param content.text string
   * @param content.image_url string
   * @param content.media_urls array[string]
   * @param content.buttons object
   * @param content.buttons.title string
   * @param content.buttons.url string
   */
  createVideoCard: function createVideoCard(session, content) {
    if (!content.media_urls || content.media_urls.length === 0) {
      throw new Error('createVideoCard: content.media_urls may not be empty.');
    }
    const videoCard = new builder.VideoCard(session);
    if (content.title) videoCard.title(content.title);
    if (content.subtitle) videoCard.subtitle(content.subtitle);
    if (content.text) videoCard.text(content.text);
    if (content.image_url) videoCard.image(builder.CardImage.create(session, content.image_url));
    const cardMedia = [];
    content.media_urls.forEach((mediaUrl) => {
      cardMedia.push({ url: mediaUrl });
    });
    videoCard.media(cardMedia);
    if (Array.isArray(content.buttons)) {
      const buttons = [];
      content.buttons.forEach((button) => {
        buttons.push(builder.CardAction.openUrl(session, button.url, button.title));
      });
      videoCard.buttons(buttons);
    }
    return new builder.Message(session)
      .attachments([videoCard]);
  },
};
