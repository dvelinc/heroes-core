'use strict';

const builder = require('botbuilder');

module.exports = {
  /**
   * Send a message directly to a user without proactive interaction by providing a previously stored address
   * @param options object { text, user, channel }
   * @param options.text string The message to send
   * @param options.sourceEvent object An uninitialized message object
   * @param options.user object { id, pgUserId }
   * @param options.user.id string The user's chatbotUserId, which is the client's app-scoped id for the user
   * @param options.user.pgUserId string The user's pgUserId, which is our internal id for the user
   * @param options.channel string The messaging channel (e.g. 'facebook')
   */
  createDirectTextMessage: function createDirectTextMessage(options) {
    if (!options.text && !options.sourceEvent) {
      throw new Error('createDirectTextMessage: options.text or options.sourceEvent are mandatory');
    }
    if (!options.user.id || !options.user.pgUserId) {
      throw new Error(
        `createDirectTextMessage: user.id and user.pgUserId may are mandatory. Are: ${options.user.id}, ${options.user.pgUserId}`  // eslint-disable-line max-len
      );
    }
    if (!options.channel) {
      throw new Error(`createDirectTextMessage: channel is mandatory. Is: ${options.channel}`);
    }
    const message = new builder.Message()
      .address({
        channelId: options.channel,
        user: { id: options.user.id },
        conversation: { id: `${options.user.id}` },
        useAuth: true,
        pgUserId: options.user.pgUserId,
      });
    if (options.text) {
      message.text(options.text);
    }
    if (options.sourceEvent) {
      message.sourceEvent(options.sourceEvent);
    }
    return message;
  },
};
