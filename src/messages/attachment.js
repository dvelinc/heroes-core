'use strict';

const builder = require('botbuilder');
const createQuickReplies = require('./quickReplies').createQuickReplies;

module.exports = {
  /**
   * Create attachment messages
   * @param session object
   * @param contentUrl string
   * @param contentType string
   * @param reusable boolean
   * @param quickReplies object Single level object. Values are taken for QR titles
   */
  createAttachmentMessage: function createAttachmentMessage(session, contentUrl, contentType, reusable, quickReplies) {
    if (!contentType) {
      throw new Error('createAttachmentMessage: contentType may not be empty.');
    }
    if (!contentUrl || (typeof contentUrl !== 'string' && !Buffer.isBuffer(contentUrl.value))) {
      throw new Error(`createAttachmentMessage: contentUrl must be a buffer or string! Is: ${contentUrl}`);
    }
    const attachment = {
      contentType,
      contentUrl,
    };
    if (reusable) {
      attachment.reusable = true;
    }
    const attachments = [attachment];
    if (quickReplies) {
      attachments.push(createQuickReplies(session, quickReplies));
    }
    return new builder.Message(session)
      .attachments(attachments);
  },
};
