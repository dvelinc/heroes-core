'use strict';

const createCard = require('./card').createCard;
const builder = require('botbuilder');
const helpers = require('../Helpers');

module.exports = {
  /**
   * Create facebook button template messages
   * @param session object
   * @param content object
   * @param content.text string
   * @param content.buttons object
   * @param content.buttons.type string
   * @param content.buttons.title string
   * @param content.buttons.payload string
   * @param content.buttons.url string
   * @param content.buttons.webview_height_ratio string
   * @param content.buttons.fallback_url string
   * @param options object { returnRaw }
   * @param options.returnRaw boolean Return an uninitialized sourceEvent object
   * @return object Initialized MBF message object
   */
  createButtons: function createButtons(session, content, options) {
    if (!Array.isArray(content.buttons)) {
      throw new Error('createButtons: content.buttons must be of type array.');
    }
    if (content.buttons.length === 0) {
      throw new Error('createButtons: content.buttons may not be empty.');
    }
    // convert to buttonTemplate carousel if max buttonCount of 3 it exceeded
    if (content.buttons.length > 3) {
      const buttonCarousel = {
        elements: [],
        carouselSize: Infinity,
      };
      const buttonsContentCopy = helpers.cloneObjectByValue(content.buttons);
      for (let i = buttonsContentCopy.length; i > 0; i -= 3) {
        const cardButtons = buttonsContentCopy.splice(0, 3);
        const buttonCard = {
          title: content.text,
          buttons: cardButtons,
        };
        buttonCarousel.elements.push(buttonCard);
      }
      buttonCarousel.quick_replies = content.quick_replies;
      return createCard(session, buttonCarousel);
    }
    // initialize message object
    const message = { facebook: {} };
    const attachment = {
      type: 'template',
      payload: {},
    };
    attachment.payload.template_type = 'button';
    if (!content.text) {
      throw new Error('createButtons: Messages to facebook must include a \'text\' property');
    }
    if (Array.isArray(content.text)) {
      attachment.payload.text = content.text[helpers.getRandomArrayKey(content.text)];
    } else {
      attachment.payload.text = content.text;
    }
    const buttons = [];
    content.buttons.forEach((button, buttonIndex) => {
      const newButton = {};
      switch (button.type) {
        case 'element_share':
          // share button
          newButton.type = button.type;
          break;
        case 'postback':
          // postback button
          if (!button.title) {
            throw new Error(`createButtons: button ${buttonIndex} is of type 'postback' but has no title set.`);
          }
          if (!button.payload) {
            throw new Error(`createButtons: button ${buttonIndex} is of type 'postback' but has not payload set.`);
          }
          newButton.type = button.type;
          newButton.title = button.title;
          newButton.payload = button.payload;
          break;
        case 'account_link':
          // account linking button
          if (!button.url) {
            throw new Error(`createButtons: button ${buttonIndex} is of type 'account_link' but has no url set.`);
          }
          newButton.type = button.type;
          newButton.url = button.url;
          break;
        default:
          // web-url button
          if (!button.title) {
            throw new Error(`createButtons: button ${buttonIndex} is of type 'web-url' but has no title set.`);
          }
          if (!button.url) {
            throw new Error(`createButtons: button ${buttonIndex} is of type 'web-url' but has no url set.`);
          }
          newButton.type = 'web_url';
          newButton.title = button.title;
          newButton.url = button.url;
          // webview button
          if (button.webview_height_ratio) {
            newButton.webview_height_ratio = button.webview_height_ratio;
            newButton.messenger_extensions = true;
          }
          if (button.fallback_url) {
            newButton.fallback_url = button.fallback_url;
          }
      }
      buttons.push(newButton);
    });
    attachment.payload.buttons = buttons;
    if (content.quick_replies) {
      const quickReplies = [];
      Object.keys(content.quick_replies).forEach((quickReply) => {
        quickReplies.push({
          content_type: 'text',
          title: content.quick_replies[quickReply],
          payload: content.quick_replies[quickReply],
        });
      });
      message.facebook.quick_replies = quickReplies;
    }
    message.facebook.attachment = attachment;
    if (options && options.returnRaw) {
      return message;
    }
    return new builder.Message(session)
      .sourceEvent(message);
  },
};
