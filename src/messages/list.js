'use strict';

const helpers = require('../Helpers');
const builder = require('botbuilder');

module.exports = {
  /**
   * Create facebook list template messages
   * @param session object
   * @param content object
   * @param content.top_element_style string
   * @param content.globalButton object { title, payload }
   * @param content.globalButton.title string
   * @param content.globalButton.payload string
   * @param content.elements array[object]
   * @param content.elements.title string
   * @param content.elements.subtitle string
   * @param content.elements.image_url string
   * @param content.elements.default_action object
   * @param content.elements.default_action.url string
   * @param content.elements.default_action.webview_height_ratio string
   * @param content.elements.default_action.fallback_url string
   * @param content.elements.buttons object
   * @param content.elements.buttons.type string
   * @param content.elements.buttons.title string
   * @param content.elements.buttons.payload string
   * @param content.elements.buttons.url string
   * @param content.elements.buttons.webview_height_ratio string
   * @param content.elements.buttons.fallback_url string
   * @param content.elements.quick_replies object { keyX: QR-titleX, keyY: QR-titleY, .. }
   */
  createList: function createList(session, content) {
    if (session.message.source !== 'facebook') {
      throw new Error('card.createList: This function is supported in Facebook Messenger only.');
    }
    if (!Array.isArray(content.elements)) {
      throw new Error('card.createList: content.elements must be an array.');
    }
    if (!content.elements || content.elements.length === 0) {
      throw new Error('card.createList: content.elements may not be empty.');
    }
    // initialize message object
    const message = {
      facebook: {},
    };
    const attachment = {
      type: 'template',
      payload: {},
    };
    attachment.payload.template_type = 'list';
    if (content.top_element_style) {
      attachment.payload.top_element_style = content.top_element_style;
    }
    if (content.elements.length > 4) {
      throw new Error(`card.createCard:  Maximal 4 elements are allowed. Are: ${content.elements.length}.`);
    }
    if (content.globalButton) {
      attachment.payload.buttons = [
        {
          type: 'postback',
          title: content.globalButton.title,
          payload: content.globalButton.payload,
        },
      ];
    }
    const elements = [];
    content.elements.forEach((element, elementsIndex) => {
      const newElement = {};
      if (!element.title) {
        throw new Error(`card.createCard: title of element ${elementsIndex} must be set.`);
      }
      if (Array.isArray(element.title)) {
        newElement.title = element.title[helpers.getRandomArrayKey(element.title)];
      } else {
        newElement.title = element.title;
      }
      if (Array.isArray(element.image_url)) {
        newElement.image_url = element.image_url[helpers.getRandomArrayKey(element.image_url)];
      } else if (typeof element.image_url === 'string') {
        newElement.image_url = element.image_url;
      }
      if (element.subtitle) newElement.subtitle = element.subtitle;
      if (element.default_action) {
        newElement.default_action = {};
        if (!element.default_action.url) {
          throw new Error(`card.createCard:  default_action of element ${element.title} has no url set.`);
        }
        if (!element.default_action.webview_height_ratio) {
          throw new Error(
            `card.createCard: default_action of element ${element.title} has no webview_height_ratio set.`); // eslint-disable-line max-len
        }
        newElement.default_action.type = 'web_url';
        newElement.default_action.url = element.default_action.url;
        newElement.default_action.webview_height_ratio = element.default_action.webview_height_ratio;
        newElement.default_action.messenger_extensions = true;
        if (element.default_action.fallback_url) {
          newElement.default_action.fallback_url = element.default_action.fallback_url;
        }
      }
      if (element.buttons && !Array.isArray(element.buttons)) {
        throw new Error(`card.createCard: buttons of element ${element.title} must be an array.`);
      }
      if (element.buttons) {
        const buttons = [];
        element.buttons.forEach((button, buttonsIndex) => {
          const newButton = {};
          switch (button.type) {
            case 'element_share':
              // share button
              newButton.type = button.type;
              break;
            case 'postback':
              // postback button
              if (!button.title) {
                throw new Error(
                  `card.createCard: button ${buttonsIndex} of element ${element.title} is of type 'postback' but has no title set.`); // eslint-disable-line max-len
              }
              if (!button.payload) {
                throw new Error(
                  `card.createCard: button ${buttonsIndex} of element ${element.title} is of type 'postback' but has not payload set.`); // eslint-disable-line max-len
              }
              newButton.type = button.type;
              newButton.title = button.title;
              newButton.payload = button.payload;
              break;
            default:
              // web-url button
              if (!button.title) {
                throw new Error(
                  `card.createCard: button ${buttonsIndex} of element ${element.title} is of type 'web-url' but has no title set.`); // eslint-disable-line max-len
              }
              if (!button.url) {
                throw new Error(
                  `card.createCard: button ${buttonsIndex} of element ${element.title} is of type 'web-url' but has no url set.`); // eslint-disable-line max-len
              }
              newButton.type = 'web_url';
              newButton.title = button.title;
              newButton.url = button.url;
              // webview button
              if (button.webview_height_ratio) {
                newButton.webview_height_ratio = button.webview_height_ratio;
                newButton.messenger_extensions = true;
              }
              if (button.fallback_url) {
                newButton.fallback_url = button.fallback_url;
              }
          }
          buttons.push(newButton);
        });
        newElement.buttons = buttons;
      }
      elements.push(newElement);
    });
    attachment.payload.elements = elements;
    message.facebook.attachment = attachment;
    if (content.quick_replies) {
      const quickReplies = [];
      Object.keys(content.quick_replies).forEach((quickReply) => {
        quickReplies.push({
          content_type: 'text',
          title: content.quick_replies[quickReply],
          payload: content.quick_replies[quickReply],
        });
      });
      message.facebook.quick_replies = quickReplies;
    }
    return new builder.Message(session)
      .sourceEvent(message);
  },
};
