'use strict';

const builder = require('botbuilder');

module.exports = {
  /**
   * Creates generic quick replies to be appended to a Message object
   * @param session object
   * @param replies object
   * @returns Keyboard object
   */
  createQuickReplies: function createQuickReplies(session, replies) {
    if (!replies || typeof replies !== 'object' || Array.isArray(replies)) {
      throw new Error(
        `createQuickReplies: replies must be of type object, is ${replies === null ? 'null' : typeof replies}`); // eslint-disable-line max-len
    }
    const result = [];
    Object.keys(replies).forEach((reply) => {
      result.push(builder.CardAction.postBack(session, replies[reply], replies[reply]));
    });
    return new builder.Keyboard().buttons(result);
  },
};
